﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MemoryCard : MonoBehaviour
{
    /* Utworzenie tablicy z frontami dostępnych kart do losowania */
    [SerializeField] Sprite [] spriteArray;

    private void Start()
    {
        transform.GetChild(0).gameObject.SetActive(true);
    }
    private void OnMouseDown()
    {
        /* Utworzenie zmiennej, która będzie odpowiadała za losowość wyświetlanej karty */
        int randomCard = Random.Range(0, spriteArray.Length);

        /* Sprawdzenie czy obiekt dziedziczony jest aktywny */
        if (transform.GetChild(0).gameObject.activeSelf)
        {
            /* Przypisanie losowej grafiki do wyświetlanej karty
              Dezaktywowanie obiektu dziedziczonego */
            GetComponent<SpriteRenderer>().sprite = spriteArray[randomCard];
            if ((GetComponent<SpriteRenderer>().sprite.name).Contains("Hearts"))
            {
                print("Hearts selected");
            }
            transform.GetChild(0).gameObject.SetActive(false);
        }
        else
        {
            /* Aktywowanie obiektu dziedziczonego */
            transform.GetChild(0).gameObject.SetActive(true);
        } 
    }
}
